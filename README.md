# dashboard_responsive

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Source

- [Flutter-Responsive-Admin-Panel-or-Dashboard](https://github.com/abuanwar072/Flutter-Responsive-Admin-Panel-or-Dashboard)

## Setup

Clone project

```
git clone https://gitlab.com/cyrilletoumi8/dashboard-admin-panel-responsive.git
```

Change directory

```
cd dashboard-admin-panel-responsive
```

Open it in VS Code

```
code .
```

Get dependency

```
flutter pub get
```

Run in debug mode

```
flutter run
```
