// ignore_for_file: prefer_const_constructors

import 'package:dashboard_responsive/config/responsive.dart';
import 'package:dashboard_responsive/controllers/menu_controller.dart';
import 'package:dashboard_responsive/screens/dashboard/dashboard_screen.dart';
import 'package:dashboard_responsive/screens/main/components/side_menu.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: context.read<MenuController>().scaffoldKet,
      drawer: SideMenu(),
      body: SafeArea(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (Responsive.isDesktop(context))
              Expanded(
                child: SideMenu(),
              ),
            Expanded(
              flex: 4,
              child: DashboardScreen(),
            ),
          ],
        ),
      ),
    );
  }
}
