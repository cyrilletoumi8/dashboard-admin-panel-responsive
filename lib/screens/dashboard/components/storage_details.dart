import 'package:dashboard_responsive/config/constants.dart';
import 'package:dashboard_responsive/config/style/primary_text.dart';
import 'package:dashboard_responsive/screens/dashboard/components/chart.dart';
import 'package:dashboard_responsive/screens/dashboard/components/storage_info_card.dart';
import 'package:flutter/material.dart';

class StorageDetail extends StatelessWidget {
  const StorageDetail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(AppConstants.defaultPadding),
      decoration: const BoxDecoration(
        color: AppColors.secondaryColor,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const PrimaryText(
            text: "Storage Details",
            size: 18,
            fontWeight: FontWeight.w500,
          ),
          const SizedBox(height: AppConstants.defaultPadding),
          const Chart(),
          StorageInfoCard(
            svgSrc: "assets/icons/Documents.svg",
            title: "Documents Files",
            amountOfFiles: "1.3GB",
            numOfFiles: 1329,
          ),
          StorageInfoCard(
            svgSrc: "assets/icons/media.svg",
            title: "Media Files",
            amountOfFiles: "15.3GB",
            numOfFiles: 1329,
          ),
          StorageInfoCard(
            svgSrc: "assets/icons/folder.svg",
            title: "Other Files",
            amountOfFiles: "1.3GB",
            numOfFiles: 1329,
          ),
          StorageInfoCard(
            svgSrc: "assets/icons/unknown.svg",
            title: "Unknown",
            amountOfFiles: "1.3GB",
            numOfFiles: 1329,
          ),
        ],
      ),
    );
  }
}
