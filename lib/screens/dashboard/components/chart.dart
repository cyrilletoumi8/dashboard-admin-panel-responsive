import 'package:dashboard_responsive/config/constants.dart';
import 'package:dashboard_responsive/config/style/primary_text.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class Chart extends StatelessWidget {
  const Chart({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: Stack(
        children: [
          PieChart(
            PieChartData(
              startDegreeOffset: -90,
              sectionsSpace: 0,
              centerSpaceRadius: 70,
              sections: piechartSelectionDatas,
            ),
          ),
          Positioned.fill(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                SizedBox(height: AppConstants.defaultPadding),
                PrimaryText(
                  text: "29.1",
                  size: 25,
                  color: AppColors.white,
                  fontWeight: FontWeight.w600,
                  height: 0.5,
                ),
                PrimaryText(
                  text: "of 128GB",
                  size: 16,
                  fontWeight: FontWeight.w300,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

List<PieChartSectionData> piechartSelectionDatas = [
  PieChartSectionData(
    color: AppColors.primaryColor,
    value: 25,
    showTitle: false,
    radius: 25,
  ),
  PieChartSectionData(
    color: Color(0xFF26E5FF),
    value: 20,
    showTitle: false,
    radius: 22,
  ),
  PieChartSectionData(
    color: Color(0xFFFFCF26),
    value: 10,
    showTitle: false,
    radius: 19,
  ),
  PieChartSectionData(
    color: Color(0xFFEE2727),
    value: 15,
    showTitle: false,
    radius: 16,
  ),
  PieChartSectionData(
    color: AppColors.primaryColor.withOpacity(0.1),
    value: 25,
    showTitle: false,
    radius: 13,
  ),
];
