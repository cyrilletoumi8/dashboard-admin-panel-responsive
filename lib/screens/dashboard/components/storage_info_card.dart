import 'package:dashboard_responsive/config/constants.dart';
import 'package:dashboard_responsive/config/style/primary_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class StorageInfoCard extends StatelessWidget {
  String title, svgSrc, amountOfFiles;
  int numOfFiles;

  StorageInfoCard({
    Key? key,
    required this.title,
    required this.svgSrc,
    required this.amountOfFiles,
    required this.numOfFiles,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(AppConstants.defaultPadding),
      margin: const EdgeInsets.only(top: AppConstants.defaultPadding),
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: AppColors.primaryColor.withOpacity(0.15),
        ),
        borderRadius: const BorderRadius.all(Radius.circular(
          AppConstants.defaultPadding,
        )),
      ),
      child: Row(
        children: [
          SizedBox(
            height: 20,
            width: 20,
            child: SvgPicture.asset(svgSrc),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: AppConstants.defaultPadding),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PrimaryText(
                    text: title,
                  ),
                  PrimaryText(
                    text: '$numOfFiles Files',
                    size: 12,
                    color: Colors.white70,
                  )
                ],
              ),
            ),
          ),
          PrimaryText(text: amountOfFiles),
        ],
      ),
    );
  }
}
