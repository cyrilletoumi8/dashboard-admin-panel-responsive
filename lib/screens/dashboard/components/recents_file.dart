import 'package:dashboard_responsive/config/constants.dart';
import 'package:dashboard_responsive/config/style/primary_text.dart';
import 'package:dashboard_responsive/models/recent_file.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RecentsFiles extends StatelessWidget {
  const RecentsFiles({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(AppConstants.defaultPadding),
      decoration: const BoxDecoration(
        color: AppColors.secondaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const PrimaryText(
            text: 'Recents Files',
            size: 15,
          ),
          SizedBox(
            width: double.infinity,
            child: DataTable(
              columnSpacing: AppConstants.defaultPadding,
              columns: const [
                DataColumn(
                  label: PrimaryText(
                    text: "File Name",
                    size: 12,
                  ),
                ),
                DataColumn(
                  label: PrimaryText(
                    text: "Date",
                    size: 12,
                  ),
                ),
                DataColumn(
                  label: PrimaryText(
                    text: "Size",
                    size: 12,
                  ),
                ),
              ],
              rows: List.generate(
                demoRecentFiles.length,
                (index) => recentFileRow(
                  demoRecentFiles[index],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

DataRow recentFileRow(RecentFile fileInfo) {
  return DataRow(
    cells: [
      DataCell(
        Row(
          children: [
            SvgPicture.asset(
              fileInfo.icon!,
              height: 30,
              width: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: AppConstants.defaultPadding,
              ),
              child: PrimaryText(
                text: fileInfo.title!,
              ),
            )
          ],
        ),
      ),
      DataCell(
        Text(fileInfo.date!),
      ),
      DataCell(
        Text(fileInfo.size!),
      ),
    ],
  );
}
