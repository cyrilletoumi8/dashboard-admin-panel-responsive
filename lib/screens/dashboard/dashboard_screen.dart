// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:dashboard_responsive/config/responsive.dart';
import 'package:dashboard_responsive/screens/dashboard/components/my_files.dart';
import 'package:dashboard_responsive/screens/dashboard/components/recents_file.dart';
import 'package:dashboard_responsive/screens/dashboard/components/storage_details.dart';
import 'package:flutter/material.dart';

import 'package:dashboard_responsive/config/constants.dart';
import 'package:dashboard_responsive/screens/dashboard/components/header.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        padding: const EdgeInsets.all(AppConstants.defaultPadding),
        child: Column(
          children: [
            Header(),
            SizedBox(height: AppConstants.defaultPadding),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 5,
                  child: Column(
                    children: [
                      MyFiles(),
                      SizedBox(height: AppConstants.defaultPadding),
                      RecentsFiles(),
                      if (Responsive.isMobile(context))
                        SizedBox(height: AppConstants.defaultPadding),
                      if (Responsive.isMobile(context)) StorageDetail(),
                    ],
                  ),
                ),
                if (!Responsive.isMobile(context))
                  SizedBox(width: AppConstants.defaultPadding),
                if (!Responsive.isMobile(context))
                  Expanded(
                    flex: 2,
                    child: StorageDetail(),
                  ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
