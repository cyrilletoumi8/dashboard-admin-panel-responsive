import 'package:flutter/material.dart';

class Responsive extends StatelessWidget {
  final Widget mobile;
  final Widget? tablet;
  final Widget desktop;
  final Widget? smallMobile;

  const Responsive({
    Key? key,
    required this.mobile,
    this.tablet,
    required this.desktop,
    this.smallMobile,
  }) : super(key: key);

  /// Determine si l'ecran courant est un petit telephone
  static bool isSmallMobile(BuildContext context) =>
      MediaQuery.of(context).size.width < 376;

  // Determine si l'ecran courant est un petit telephone est un telephone
  static bool isMobile(BuildContext context) =>
      MediaQuery.of(context).size.width < 850 &&
      MediaQuery.of(context).size.width >= 376;

  // Determine si l'ecran courant est un petit telephone est une tablet
  static bool isTablet(BuildContext context) =>
      MediaQuery.of(context).size.width < 1200 &&
      MediaQuery.of(context).size.width >= 850;

  // Determine si l'ecran courant est un petit telephone est un Desktop( Ordinateur, TV, etc)
  static bool isDesktop(BuildContext context) =>
      MediaQuery.of(context).size.width >= 1200;

  @override
  Widget build(BuildContext context) {
    // On recupere la largeur de l'appareil
    final size = MediaQuery.of(context).size;

    /** 
     * Si notre largeur est plus de 1200 alors on considère que c'est un desktop
     */
    if (size.width >= 1200) {
      return desktop;
    }

    /**
     * Si la largeur est moin de 1200 et plus de 850 alors on considère que c'est une tablet
     */
    else if (size.width >= 850 && tablet != null) {
      return tablet!;
    }

    /**
     * Si la largeur est plus de 376 et moin de 850 alors on considère que c'est un mobile
     */
    else if (size.width >= 376 && size.width <= 850) {
      return mobile;
    }

    /**
     * Au cas contraire on considere que c'est un petit mobile
     */
    else {
      return smallMobile!;
    }
  }
}
