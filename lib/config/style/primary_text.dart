import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class PrimaryText extends StatelessWidget {
  final String text;
  final double? size;
  final FontWeight? fontWeight;
  final Color? color;
  final double? height;

  const PrimaryText({
    Key? key,
    required this.text,
    this.size,
    this.fontWeight,
    this.color,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: GoogleFonts.outfit(
        color: color,
        height: height,
        fontWeight: fontWeight,
        fontSize: size,
      ),
    );
  }
}
