import 'package:flutter/material.dart';

class AppColors {
  static const primaryColor = Color(0xFF2697FF);
  static const secondaryColor = Color(0xFF2A2D3E);
  static const bgColor = Color(0xFF212332);
  static const white = Colors.white;
}

class AppConstants {
  static const appName = "E-Billing";
  static const defaultPadding = 16.0;
}
